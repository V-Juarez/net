<h1>Introducción a C#</h1>

<h1>Ricardo Celis</h1>

<h1>Table of Contends</h1>

- [1. Introducción a C](#1-introducción-a-c)
  - [¿Por qué aprender C#? ¿Para qué sirve?](#por-qué-aprender-c-para-qué-sirve)
  - [Instalación de Visual Studio Community](#instalación-de-visual-studio-community)
  - [Conoce .NET Framework 6](#conoce-net-framework-6)
  - [Introducción a Visual Studio y nuestro primer "Hola, mundo" en consola](#introducción-a-visual-studio-y-nuestro-primer-hola-mundo-en-consola)
  - [¡Nuestro primer "Hola, mundo" en una aplicación gráfica!](#nuestro-primer-hola-mundo-en-una-aplicación-gráfica)
  - [¿Cómo funcionan los Namespaces en C#?](#cómo-funcionan-los-namespaces-en-c)
- [2. Lógica de programación](#2-lógica-de-programación)
  - [Tipos de datos](#tipos-de-datos)
  - [Variables](#variables)
  - [Manejo de strings](#manejo-de-strings)
  - [Trabajando con números y operadores aritméticos](#trabajando-con-números-y-operadores-aritméticos)
  - [Operadores lógicos](#operadores-lógicos)
  - [Operadores relacionales](#operadores-relacionales)
  - [Cómo leer datos de un usuario en C](#cómo-leer-datos-de-un-usuario-en-c)
  - [Arreglos en C](#arreglos-en-c)
  - [Listas](#listas)
  - [Métodos o methods](#métodos-o-methods)
  - [Métodos de strings](#métodos-de-strings)
  - [Cómo crear tus propios métodos](#cómo-crear-tus-propios-métodos)
- [3. Bucles y estructuras de control en C](#3-bucles-y-estructuras-de-control-en-c)
  - [La sentencia if](#la-sentencia-if)
  - [La sentencia switch](#la-sentencia-switch)
  - [Ciclo for](#ciclo-for)
  - [Ciclo while](#ciclo-while)
- [4. Proyecto](#4-proyecto)
  - [Introducción del proyecto: sistema de registros de usuarios](#introducción-del-proyecto-sistema-de-registros-de-usuarios)
  - [Creando la búsqueda y el registro de usuarios](#creando-la-búsqueda-y-el-registro-de-usuarios)
  - [Finalizado del proyecto: mostrando la lista de usuarios registrados](#finalizado-del-proyecto-mostrando-la-lista-de-usuarios-registrados)
- [5. Tus siguientes pasos con C](#5-tus-siguientes-pasos-con-c)
  - [POO: tu siguiente paso con C](#poo-tu-siguiente-paso-con-c)

# 1. Introducción a C#

## ¿Por qué aprender C#? ¿Para qué sirve?

Puedes crear videoJuegos y web. C# es mutliplataforma.

## Instalación de Visual Studio Community

El IDE por excelencia es VS para desarrollar en .NET, sin embargo, también pueden hacerlo en VSCode para proyectos de .NET Core instalando algunas extensiones.

- Aquí les dejo la documentación para usar C# en [VSCode](https://code.visualstudio.com/docs/languages/csharp)

**Visual Studio Code**
Nos va ayudar en temas que tiene que ver con la web, es un **EDITOR DE CÓDIGO** que nos ayudara en todos los lenguajes que sirven en todas las plataformas.

**Visual Studio**
Es mas robusto, permite hacer interfaz grafica, es un **IDE**, esta enfocado en cargas de trabajo web, escritorio, móvil, juegos y otros.

**Workloads (Cargas de trabajo)**
Es el flujo o las herramientas que se va necesitar usar día a dia.

- ASP. NET and web development
- .NET desktop development

[![img](https://www.google.com/s2/favicons?domain=https://visualstudio.microsoft.com/wp-content/uploads/2017/02/Microsoft-favicon.png)Visual Studio 2022 Community Edition: descargar la versión gratuita más reciente](https://visualstudio.microsoft.com/es/vs/community/)

## Conoce .NET Framework 6

las versiones de .NET:

- .NET Framework (4.8) = Apicaciones solo para Windows
- .NET Core (3.1) = Version OpenSource que es multiplataforma.
- .NET 5 (y luego la 6) = Version más reciente que unifica .Net Core con .NET Framework, opensource y multiplataforma.

En la clase anterior acabamos de instalar Visual Studio Community 2019. Sin embargo, Microsoft recientemente lanzó la versión 2022, pero… ¿qué cambia exactamente? ¿Me impedirá seguir el curso? ¿Debo instalar otra versión? 🤔… Bien, comparemos estas versiones 😉. **Spoiler:** Esto no te impedirá continuar con el curso, pero te enseñaré a trabajar con esta versión 👀.

En la versión 2019 de Visual Studio usamos .NET Framework en su versión 3.1. Entre las principales características de esta versión resalta el hecho de que debemos escribir toda la estructura de clases y namespaces en nuestro programa:

![carbon.png](https://static.platzi.com/media/user_upload/carbon-d6a5cfa9-d425-40da-ae08-9c105ea1c3ab.jpg)

¡Pero esto ya no es necesario en Visual Studio 2022! Esta versión del IDE utiliza por defecto a .NET Framework en su versión 6, y es gracias a esta versión que podemos empezar a escribir directamente nuestro programa, sin tener la estructura base 😎:

![carbon (1).png](https://static.platzi.com/media/user_upload/carbon%20%281%29-33fe7408-84f0-4951-bf8c-01cbbcbb1cbd.jpg)

Aunque .NET Framework permite esto, algunas veces querrás trabajar con diferentes namespaces, o incluso querrás tener el control por tu propia cuenta de la clase con la que quieres trabajar; es por eso que también tienes la posibilidad de escribir la estructura de clases de siempre para que puedas tener todo el control de tu programa.

## ¿Y qué pasa si yo quiero usar la versión que usa el profesor en las clases?

¡Eso no es ningún problema! Como ya te mencioné, esa es la versión 3.1 de .Net Framework, esta versión ya viene por defecto en Visual Studio 2019, pero si estás usando Visual Studio 2022 la puedes descargar… ¿adivinas de dónde? 👀.

Cuando instalamos Visual Studio también se nos instaló una aplicación llamada “Visual Studio Installer”. En esta aplicación tú puedes añadir o quitar funcionalidades de tu IDE, pero eso lo profundizaremos en la próxima clase. Por ahora, busca esa aplicación dentro de tus programas y ábrela.

Dentro de ella debemos ir a la pestaña “Componentes Individuales” y aquí deberás seleccionar la opción que dice “.NET Core 3.1 Runtime (LTS)”

![Captura de pantalla 2021-12-11 184013.png](https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%202021-12-11%20184013-52714213-1e8e-4031-88bb-65e6bad58f0c.jpg)

Una vez seleccionada debes hacer click sobre el botón “Modify” o “Modificar” y esto añadirá .NET Core 3.1 a tu IDE, simplemente debes reiniciarlo cuando termine.

Con esto listo, cuando crees un nuevo proyecto o solución, en la ventana de configuración de tu nuevo proyecto verás una opción que dice “Framework”. En ese campo podrás seleccionar entre la versión 3.1 o la versión 6 (o cualquier otra versión que hayas instalado).

![configure-new-project-framework.png](https://static.platzi.com/media/user_upload/configure-new-project-framework-ee29a4af-adcf-4a04-9524-018cb5449859.jpg)

Recuerda que la versión 3.1 es la que estaremos usando durante la mayoría de clases el curso, pero tú puedes usar la versión 6 sin problemas 😉.

Como dato adicional, el proyecto que encontrarás en la sección de recursos de cada una de las clases estará hecho bajo la versión 3.1, esto para que sea compatible con quienes aún use la versión 2019 💚.

👋 Nos vemos en la siguiente clase donde seguiremos profundizando en nuestro IDE.

## Introducción a Visual Studio y nuestro primer "Hola, mundo" en consola

### Instrucciones para los usuarios de Linux

### Creando proyecto de consola

Si ya tienes instalado dotnet-sdk, puedes crear un proyecto de consola con el siguiente comando:

```sh
dotnet new console
```

El comando utilizará el nombre de la carpeta en la que te encuentres como nombre para la clase principal del proyecto.

Cabe resaltar que, a diferencia de Visual Studio, el comando no crea el archivo de solución, sino que únicamente crea el archivo del proyecto. Pueden crearlo manualmente si gustan.

### Ejecutar el proyecto

Ejecutar nuestro proyecto es bastante simple. Se posicionan en la carpeta del proyecto en su terminal y ejecutan:

```sh
dotnet run
```

Y listo, podrán ver su “Hello World!” en la consola.

## ¡Nuestro primer "Hola, mundo" en una aplicación gráfica!

## ¿Cómo funcionan los Namespaces en C#?

# 2. Lógica de programación

## Tipos de datos

## Variables

## Manejo de strings

## Trabajando con números y operadores aritméticos

## Operadores lógicos

## Operadores relacionales

## Cómo leer datos de un usuario en C#

## Arreglos en C#

## Listas

## Métodos o methods

## Métodos de strings

## Cómo crear tus propios métodos

# 3. Bucles y estructuras de control en C#

## La sentencia if

## La sentencia switch

## Ciclo for

## Ciclo while

# 4. Proyecto

## Introducción del proyecto: sistema de registros de usuarios

## Creando la búsqueda y el registro de usuarios

## Finalizado del proyecto: mostrando la lista de usuarios registrados

# 5. Tus siguientes pasos con C#

## POO: tu siguiente paso con C#
